module.exports = function sortByNumOfVitContains(items){
    return items.sort((a,b)=>{
        if(a["contains"].length>b["contains"].length){
            return -1;
        }else if(a["contains"].length<b["contains"].length){
            return 1;
        }
        return 0;
    })
}