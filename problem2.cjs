module.exports = function getItemsWithVitaminC(items){
    return items.filter(item=>item["contains"].contains("Vitamin C"))
}