module.exports = function groupItemBasedOnVitamins(items){
    return items.reduce((group, item)=>{

        item["contains"].split(", ").forEach(vitamin=>{
            if(group[vitamin] === undefined){
                group[vitamin] = [item["name"]]
            }else{
                group[vitamin].push(item["name"])
            }
        });
        return group;
    }, {});
}
