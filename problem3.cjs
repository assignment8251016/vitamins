module.exports = function getItemsWithVitaminA(items){
    return items.filter(item=>item["contains"].includes("Vitamin A"))
}